﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathManager : MonoBehaviour
{

    public GameObject pathFinderPrefab;

    public GameObject[] targets;
    public GameObject[] pathFinders;

    [SerializeField] private KeyCode SmellsButton;
    
    void Start()
    {
        targets = GameObject.FindGameObjectsWithTag("Target");
        pathFinders = new GameObject[targets.Length];
        for (int i = 0; i < targets.Length; i++)
        {
            GameObject newPathFinder = Instantiate(pathFinderPrefab, Vector3.zero, Quaternion.identity);
            newPathFinder.GetComponent<PathFinder>().Target = targets[i];
            //newPathFinder.SetActive(false);
            pathFinders[i] = newPathFinder;
        }
    }


    private void Update()
    {
        if (Input.GetKeyDown(SmellsButton))
        {
            for (int i = 0; i < pathFinders.Length; i++)
            {
                pathFinders[i].GetComponent<PathFinder>().RestartDraw(transform.position);
            }
        }
    }
}
