﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class Controller : MonoBehaviour
{
	private Rigidbody rigidbody;
	private Camera viewCamera;
    private Vector3 velocity;
    private PhotonView m_pPhotonView;
    private CameraController cameraController;

    public float moveSpeed = 6f;
    public GameObject m_pBulletPrefab;
    public float m_pLifeManager;
    public bool isControlled;
	private void Awake()
	{
        m_pPhotonView = GetComponent<PhotonView>();
        rigidbody = GetComponent<Rigidbody>();
        viewCamera = Camera.main;

        if (m_pPhotonView.IsMine)
            cameraController = FindObjectOfType<CameraController>();

    }

    // Start is called before the first frame update
    void Start()
    {
        m_pLifeManager = 100;
        if (m_pPhotonView.IsMine)
        {
            isControlled = true;

            // m_pLifeManager = FindObjectOfType<LifeManager>();
            //  m_pLifeManager.Init();
            cameraController.SetTarget(transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!m_pPhotonView.IsMine || !isControlled)
            return;

        if(Input.GetKeyDown(KeyCode.Mouse0))
            m_pPhotonView.RPC("Fire", RpcTarget.AllViaServer, rigidbody.position, rigidbody.rotation);

        velocity = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized * moveSpeed;
    }

    private void FixedUpdate()
    {
        if (!m_pPhotonView.IsMine || !isControlled)
            return;

        Vector3 mousePosition = viewCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, viewCamera.transform.position.y));

        transform.LookAt(mousePosition + Vector3.up * transform.position.y);

        if(velocity != Vector3.zero)
            rigidbody.velocity = Vector3.zero;

        rigidbody.MovePosition(rigidbody.position + velocity * Time.fixedDeltaTime);
    }

    private IEnumerator WaitForRespawn()
    {
        yield return new WaitForSeconds(ZombieGame.PLAYER_RESPAWN_TIME);

        m_pPhotonView.RPC("RespawnZombie", RpcTarget.AllViaServer);
    }

    [PunRPC]
    public void RespawnZombie()
    {
        rigidbody.useGravity = true;
        GetComponent<Collider>().enabled = true;
        GetComponent<Renderer>().enabled = true;
        m_pLifeManager = 100;
        isControlled = true;
    }
    [PunRPC]
    public void DestroyZombie()
    {
        rigidbody.useGravity = false;
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;

        GetComponent<Collider>().enabled = false;
        GetComponent<Renderer>().enabled = false;

        isControlled = false;

        
        if (m_pPhotonView.IsMine)
        {
         
            StartCoroutine("WaitForRespawn");
                
        }
    }
    public void TakeDamageEvent(float pDamage)
    {
        m_pLifeManager -= pDamage;
        if (m_pLifeManager <= 0)
        {
            m_pPhotonView.RPC("DestroyZombie", RpcTarget.All); 
            //   StartCoroutine(WaitForRespawn());
        }
        //  m_pPhotonView.RPC("TakeDamage", RpcTarget.All, pDamage);
    }
    [PunRPC]
    private void TakeDamage(float pDamage)
    {
        m_pLifeManager -= pDamage;
        if (m_pLifeManager <= 0)
        {
            StartCoroutine(WaitForRespawn());
        }
    }
    [PunRPC]
    public void Fire(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        float lag = (float)(PhotonNetwork.Time - info.SentServerTime);
        GameObject bullet;

        /** Use this if you want to fire one bullet at a time **/
        bullet = Instantiate(m_pBulletPrefab, rigidbody.position, Quaternion.identity) as GameObject;
        bullet.GetComponent<Bullet>().InitializeBullet(m_pPhotonView.Owner, (rotation * Vector3.forward), Mathf.Abs(lag));


        /** Use this if you want to fire two bullets at once **/
        //Vector3 baseX = rotation * Vector3.right;
        //Vector3 baseZ = rotation * Vector3.forward;

        //Vector3 offsetLeft = -1.5f * baseX - 0.5f * baseZ;
        //Vector3 offsetRight = 1.5f * baseX - 0.5f * baseZ;

        //bullet = Instantiate(BulletPrefab, rigidbody.position + offsetLeft, Quaternion.identity) as GameObject;
        //bullet.GetComponent<Bullet>().InitializeBullet(photonView.Owner, baseZ, Mathf.Abs(lag));
        //bullet = Instantiate(BulletPrefab, rigidbody.position + offsetRight, Quaternion.identity) as GameObject;
        //bullet.GetComponent<Bullet>().InitializeBullet(photonView.Owner, baseZ, Mathf.Abs(lag));
    }
}
