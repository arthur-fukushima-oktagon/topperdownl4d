﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeManager : MonoBehaviour
{

    [SerializeField]
    private float lifeBar;

    public float LifeBar { get => lifeBar; set => lifeBar = value; }



    // Start is called before the first frame update
    public void Init()
    {
        LifeBar = 100;
    }
    [PunRPC]
    public void TakeDamage(float damage)
    {
        LifeBar -= damage;
    }

    public void Heal(float healAmount)
    {
        LifeBar += healAmount;
    }
    
}
