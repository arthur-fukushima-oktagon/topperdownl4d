﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RadarFinder : MonoBehaviour
{

    [SerializeField]
    private GameObject _PingPrefab;

    private List<GameObject> pingPoolList;
    private List<Vector3> spawnPositions;

    [SerializeField]
    private KeyCode ActiveRadarButton;
    
    // Start is called before the first frame update
    void Start()
    {
        pingPoolList = new List<GameObject>();
        spawnPositions  = new List<Vector3>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(ActiveRadarButton))
        {
            RadarPing();
        }
    }

    void CreatePing()
    {
        GameObject tPing;
        tPing = Instantiate(_PingPrefab, Vector3.zero, Quaternion.identity);
        tPing.SetActive(false);
        pingPoolList.Add(tPing);
    }


    void RadarPing()
    {
        RaycastHit[] targetsInViewRadius = Physics.SphereCastAll(transform.position, 360f, Vector3.fwd, 300f);


        spawnPositions.Clear();
        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            if (targetsInViewRadius[i].transform.CompareTag("Target"))
            {
                spawnPositions.Add(targetsInViewRadius[i].transform.position);
            }
        }

        if (pingPoolList.Count < spawnPositions.Count)
        {
            Debug.Log(pingPoolList.Count);
            Debug.Log(spawnPositions.Count);
            while (pingPoolList.Count < spawnPositions.Count)
            {
                CreatePing();
            }
        }

        for (int i = 0; i < spawnPositions.Count; i++)
        {
            pingPoolList[i].transform.position = spawnPositions[i];
            pingPoolList[i].SetActive(true);
        }
        
    }
    
}
