﻿using UnityEngine;

/// <summary>
/// Simple Camera movement script.
/// </summary>
public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Transform _target;

    private readonly float _smoothTime = 0.250f;

    [Range(0f, 100f)]
    public float yOffset = -10.0f;

    private Vector3 _targetPostion;
    private Vector3 _velocity;

    /// <summary>
    /// Smoothly follow the player.
    /// </summary>
    private void LateUpdate()
    {
        if (_target != null)
        {
            var targetPos = new Vector3(_target.position.x, yOffset, _target.position.z);
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref _velocity, _smoothTime);
        }
    }

    public void SetTarget(Transform pTarget)
    {
        _target = pTarget;
    }
}