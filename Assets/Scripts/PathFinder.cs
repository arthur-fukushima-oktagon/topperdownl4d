﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathFinder : MonoBehaviour
{
    private NavMeshAgent agent;
    
    private GameObject target;

    private float alpha;

    private float deltaTime;

    public float pathDuration;
    
    public GameObject Target
    {
        get { return target; }
        set { target = value; }
    }

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        gameObject.SetActive(false);
    }

    void DrawPath()
    {
        if( agent == null || agent.path == null )
            return;
 
        var line = this.GetComponent<LineRenderer>();
        Color actualColor = line.material.color;
        
        
        line.material.SetColor(0, new Color(actualColor.r, actualColor.g, actualColor.b, alpha));
 
        var path = agent.path;
 
        line.SetVertexCount( path.corners.Length );
 
        for( int i = 0; i < path.corners.Length; i++ )
        {
            line.SetPosition( i, path.corners[ i ] );
        }
    }

    public void RestartDraw(Vector3 initialLocation)
    {
        transform.position = initialLocation;
        DrawPath();
        gameObject.SetActive(true);
        deltaTime = 0f;
    }

    void Update()
    {

        if (deltaTime <= pathDuration)
        {
          //  agent.destination = target.transform.position;
        
          //  DrawPath();

            deltaTime += Time.deltaTime;
        }
        else
        {
            gameObject.SetActive(false);
        }
        
        
    }
    
    
    
}
