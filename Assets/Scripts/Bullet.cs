﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Bullet : MonoBehaviour
{
    public Player Owner { get; private set; }

    public void Start()
    {
        Destroy(gameObject, 7.0f);
    }

    public void OnTriggerEnter(Collider other)
    {
        Controller tLifeManager = other.gameObject.GetComponent<Controller>();
        if (tLifeManager != null)
        {
            //    if(Owner.TagObject.ToString() != other.tag.ToString())
            //   {
           tLifeManager.TakeDamageEvent(10);
         //   }
        }
        Destroy(gameObject);
    }

    public void InitializeBullet(Player owner, Vector3 originalDirection, float lag)
    {
        Owner = owner;

        transform.forward = originalDirection;

        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = originalDirection * 50.0f;
        rigidbody.position += rigidbody.velocity * lag;
    }
}
