﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class AbilityRoll : MonoBehaviour
{
    private Rigidbody cachedRigidbody;
    private PhotonView m_pPhotonView;
    private Camera viewCamera;

    public KeyCode key;
    public float rollSpeed = 20f;

    private void Awake()
    {
        if (cachedRigidbody == null)
            cachedRigidbody = GetComponent<Rigidbody>();

        if(m_pPhotonView == null)
            m_pPhotonView = GetComponent<PhotonView>();

        viewCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if(m_pPhotonView.IsMine)
        {
            if(Input.GetKeyDown(key))
            {
                Vector3 mousePosition = viewCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, viewCamera.transform.position.y));

                mousePosition.y = transform.position.y;

                Vector3 direction = mousePosition - transform.position;

                Debug.DrawRay(transform.position, mousePosition - transform.position, Color.red, 2f);

                //cachedRigidbody.MovePosition(cachedRigidbody.position + mousePosition.normalized * Time.fixedDeltaTime * rollSpeed);

                cachedRigidbody.AddForce(direction.normalized * rollSpeed);
            }
        }
    }
}
